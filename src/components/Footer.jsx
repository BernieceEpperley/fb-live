import React, { Component } from 'react'

class Footer extends Component {
	render() {
		let d = new Date();

		return(
		<div className="Footer">
			<hr/>
			<p>
			&copy;{d.getFullYear()} <a href="https://nedpals.xyz">Ned Palacios</a> |&nbsp;
			<a href="https://gitlab.com/nedpals/fb-live">Project Repo</a> | 
			Powered by <a href="https://reactjs.org">React</a>.
			</p>
		</div>
		);

	}
}

export default Footer;